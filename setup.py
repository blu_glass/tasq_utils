from pkg_resources import parse_requirements
from setuptools import setup, find_packages


with open('requirements.txt', 'r') as f:
    requirements = [str(ir) for ir in parse_requirements(f)]

with open("README.md", "r") as f:
    long_description = f.read()

packages = find_packages()

setup(
    name='celletasq',
    version='0.0.0.7',
    packages=packages,
    package_dir={"celletasq": "./celletasq"},
    package_data={"": []},
    include_package_data=True,
    install_requires=requirements,
    description="CTO Tasq utils",
    long_description=long_description,
    long_description_content_type="text/markdown"
)
