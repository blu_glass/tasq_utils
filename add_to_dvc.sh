#!/bin/bash

git clone git@bitbucket.org:blu_glass/cto_dvc_datasets.git
cd cto_dvc_datasets || exit
mkdir -p "$1"
cp -r "$0" "$1"
dvc add "$0" --file "$2"
git add "$2"
git commit -m "Added data from TasQ: $2"
git push origin master
dvc push
