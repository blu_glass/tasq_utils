import click

from celletasq.core import read_tasq_json, tasq_json_to_tasq_df, tasq_df_to_act_dataset


@click.group()
def cli():
    pass


@cli.command()
@click.argument('json_path')
@click.argument('dataset_path')
@click.option('--class_type', default="single_class", help='single_class / multi_class')
def tasq_to_act_dataset(json_path, dataset_path):
    tasq_json = read_tasq_json(json_path)
    tasq_df = tasq_json_to_tasq_df(tasq_json)
    tasq_df_to_act_dataset(tasq_df, dataset_path)


if __name__ == "__main__":
    cli()
