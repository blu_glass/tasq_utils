# TasQ utils

Utilities for TasQ

## Setup

Make sure you are inside venv.

```bash
pip install -r requirements.txt
```

## Usage

### Converting TasQ json to ACT dataset

```bash
python cli.py tasq_to_act_dataset /path/to/tasq.json /where/to/put/act/dataset_name single_class
```

This command will download images referenced by entries in exported json and organize them in a manner acceptable by our ACT library.

### Adding data to DVC and pushing to remote

```bash
sh add_to_dvc.sh /where/to/put/act/dataset_name act/ act_dataset_name.dvc
```

Arguments explained:

- `$0`: path to the dataset, should be the one you've supplied to tasq_to_act_dataset as second arg
- `$1`: RELATIVE path indicating where to put the dataset inside the DVC workspace, e.g. `act/` where resulting dataset would reside in `act/dataste_name`
- `$2`: name you want to give to the generated DVC file

This works for both creating new datasets and extending existing ones.
