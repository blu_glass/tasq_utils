import os
import json
import pandas as pd
import concurrent.futures
import requests


def read_tasq_json(json_path):
    with open(json_path) as f:
        return json.load(f)


def tasq_json_to_tasq_df(tasq_json, skip_not_annotated=True):
    records = []

    for r in tasq_json:
        if skip_not_annotated and not r['annotations']:
            continue

        record = {
            'url': r['resource']['url'],
            'filename': r['resource']['body']['file_name'],
            'label': None
        }

        if r['annotations']:
            most_voted_annotation = sorted(r['annotations'], key=lambda x: x['voteCount'], reverse=True)
            record['label'] = most_voted_annotation[0]['body']['label']
            record['votes'] = most_voted_annotation[0]['voteCount']

        records.append(record)

    return pd.DataFrame.from_records(records)


def ensure_folder_structure(dataset_path):
    images_path = os.path.join(dataset_path, "images")
    catalogues_path = os.path.join(dataset_path, "catalogues")
    fv_path = os.path.join(dataset_path, "feature_vectors")

    for p in [dataset_path, images_path, catalogues_path, fv_path]:
        if not os.path.exists(p):
            os.makedirs(p, exist_ok=True)


def download_image(image_url, output_path):
    p, fname = os.path.split(output_path)
    os.makedirs(p, exist_ok=True)
    r = requests.get(image_url, allow_redirects=True)
    open(output_path, 'wb').write(r.content)


def download_images_concurrently(url_list, output_root, download_max_workers):
    with concurrent.futures.ThreadPoolExecutor(max_workers=download_max_workers) as executor:
        future_to_url = dict(
            (executor.submit(download_image, url, os.path.join(output_root, os.path.split(url)[1])), url)
            for url in url_list)

        for future in concurrent.futures.as_completed(future_to_url):
            url = future_to_url[future]
            if future.exception() is not None:
                print(f'{url} generated an exception: {future.exception()}')


def tasq_df_to_act_dataset(tasq_df, dataset_path, class_type="single_class", download_max_workers=10):
    ensure_folder_structure(dataset_path)

    images_path = os.path.join(dataset_path, "images")

    if class_type == "single_class":
        positive_urls = tasq_df[tasq_df['label'] == 'Yes']['url'].values
        negative_urls = tasq_df[tasq_df['label'] == 'No']['url'].values

        print(f'Downloading {len(positive_urls)} positive images')
        download_images_concurrently(positive_urls, os.path.join(images_path, "positive"), download_max_workers)

        print(f'Downloading {len(negative_urls)} negative images')
        download_images_concurrently(negative_urls, os.path.join(images_path, "negative"), download_max_workers)

    elif class_type == "multi_class":
        for label in tasq_df['label'].unique():
            urls = tasq_df[tasq_df['label'] == label]['url'].values

            print(f'Downloading {len(urls)} {label} images')
            download_images_concurrently(urls, os.path.join(images_path, label), download_max_workers)
    else:
        raise Exception(f"Supplied invalid value for class_type: {class_type}")

    return dataset_path
